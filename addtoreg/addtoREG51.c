#include<reg51.h>
void delay1(unsigned int n)
{
	unsigned int i;
	TMOD = 0x10;
	for(i=0;i<n;i++)
	{
		TH1 = 0xFC;
		TL1 = 0x18;
		TR1 = 1;
		while(TF1==0);
		TF1 = 0;
		TR1 = 0;
	}
}

void main()
{
	while(1)
	{
	P1 = 0xFF;
	delay1(1000);
	P1 = 0x00;
	delay1(1000);
	}
}