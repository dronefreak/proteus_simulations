#include<reg51.h>
sbit led = P1^0;
void led_off() interrupt 0
{
	led = 0;
	delay(50);
}
void main()
{
	IE = 0x81;
	led = 1;
}