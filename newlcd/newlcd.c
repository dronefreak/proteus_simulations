#include<reg51.h>
sbit rs = P3^0;
sbit rw = P3^1;
sbit en = P3^2;
void cmd(unsigned char value, z)
{
	P2 = value;
	rs = z;
	rw = 0;
	en =1;
	delay(1);
	en = 0;
}
void display(unsigned long b)
{
unsigned long i,store=b,c=0;	
cmd(0x04,0);
	if(b==0)
	{
		cmd('0',1);
	}
while(1)
{
	while(store!=0)
	{
		i = b%10;
		++c;
		store = store/10;
	}
	cmd((0x80+c-1),0);
	while(b>0)
	{
		i = b%10;
		cmd((i+48),1);
		b = b/10;
	}
	break;
}
}
void main()
{
	unsigned long c=0;
	cmd(0x38,0);
		cmd(0x0E,0);
		cmd(0x06,0);
		cmd(0x80,0);
	while(1)
	{
		display(c);
		++c;
	}
}