//this code is for IC AT89C51 by ATmel, and NOT for 8086.....Convert the code to asm, and change the pin config accordingly


#include<reg51.h>
#define lcd P2 //Port 2 of at89c51 is used to connect to 8 data pins of LCD module
sbit rs = P3^0; //connected to pin 3.0 or any other pin
sbit rw = P3^1; // connected to pin 3.1 or any other pin
sbit en = P3^2; //connected to pin 3.2 or any other pin 
void delay(unsigned int x) //function for creating delay
{
unsigned int i,j;
for(i=0;i<1000;i++)
for(j=0;j<x;j++);
}
void main() //main code
{
lcd = 0x38; //Port 2 is sent data hex 0x38, this command initiates the pixels
rs = 0; // this operation is not read command hence rs = 0
rw = 0; //always be zero
en = 1; // this sequence of en  = 1 followed by delay and en = 0, is permanent, has to be written always, after every operation, whether rs type or not
delay(10);
en = 0;
lcd = 0x0E; //Port 2 is sent this data specifies position of the cursor
rs = 0;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 0x06;//Cursor moves right by 1 position, and not left
rs = 0;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 0x80;//Characters would start printing from row 1 column 1
rs = 0;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'B'; port 2 is sent char B, which is diaplayed
rs = 1; //operation is, reading prestored data in the LCD internal storage, hence rs = 1
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'I';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'T';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'S';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = ' ';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'P';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'i';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'l';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'a';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'n';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
lcd = 'I';
rs = 1;
rw = 0;
en = 1;
delay(10);
en = 0;
}