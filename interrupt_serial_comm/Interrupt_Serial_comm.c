#include<reg51.h>
sbit led = P1^0;
void led_off() interrupt 4
{
	led = 0;
	delay(50);
}
void main()
{
	cmd(0x38,0);
	cmd(0x0E,0);
	cmd(0x06,0);
	cmd(0x80,0);
	IE = 0x90;
	cmd('r',1);
	delay(100);
	led = 1;
}