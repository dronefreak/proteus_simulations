#include<reg51.h>
sbit a = P2^4;
sbit b = P2^5;
sbit c = P2^6;
sbit d = P2^7;
sbit rs = P3^0;
sbit rw = P3^1;
sbit en = P3^2;
void cmd(unsigned char value, z)
{
	P1 = value;
	rs = z;
	rw = 0;
	en = 1;
	delay(5);
	en = 0;
}
void display(unsigned long b)
{
unsigned long i,store=b,c=0;	
cmd(0x04,0);
	if(b==0)
	{
		cmd('0',1);
	}
while(1)
{
	while(store!=0)
	{
		i = b%10;
		++c;
		store = store/10;
	}
	cmd((0xC0+c-1),0);
	while(b>0)
	{
		i = b%10;
		cmd((i+48),1);
		b = b/10;
	}
}
}
void answer(unsigned char *num1, unsigned char *num2, unsigned char op)
{
	unsigned long number1 = 0, number2 = 0, ans,tens=1;
	unsigned char s1,s2,k;
	for(s1=0;num1[s1]!='\0';++s1);
	for(s2=0;num1[s2]!='\0';++s2);
	for(k=s1;k>=0;--k)
	{
		number1 = number1 + (num1[k]-48)*tens;
		tens = tens*10;
	}
	tens = 1;
	for(k=s2;k>=0;--k)
	{
		number2 = number2 + (num2[k]-48)*tens;
		tens = tens*10;
	}
	if(op=='+')
	{
		ans = number1 + number2;
		display(ans);
	}
	if(op=='-')
	{
		ans = number1 - number2;
		display(ans);
	}
	if(op=='x')
	{
		ans = number1*number2;
		display(ans);
	}
	if(op=='/')
	{
		ans = number1/number2;
		display(ans);
	}
}
void main()
{
	unsigned char num1[8],num2[8],i=0,op;
	cmd(0x38,0);
	cmd(0x0E,0);
	cmd(0x06,0);
	cmd(0x80,0);
	while(1)
	{
	P2 = 0xFE;
	if(a==0)
	{
		num1[i] = '7';
		++i;
		cmd('7',1);
		delay(26);
	}
	if(b==0)
	{
		num1[i] = '8';
		++i;
		cmd('8',1);
		delay(26);
	}
	if(c==0)
	{
		num1[i] = '9';
		++i;
		cmd('9',1);
		delay(26);
	}
	if(d==0)
	{
		op = '/';
		cmd('/',1);
		delay(26);
				i=0;
		P2 = 0xFE;
			if(a==0)
			{
				num2[i] = '7';
				++i;
				cmd('7',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '8';
				++i;
				cmd('8',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '9';
				++i;
				cmd('9',1);
				delay(26);
			}
			P2 = 0xFD;
			if(a==0)
			{
				num2[i] = '4';
				++i;
				cmd('4',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '5';
				++i;
				cmd('5',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '6';
				++i;
				cmd('6',1);
				delay(26);
			}
			P2 = 0xFB;
			if(a==0)
			{
				num2[i] = '1';
				++i;
				cmd('1',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '2';
				++i;
				cmd('2',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '3';
				++i;
				cmd('3',1);
				delay(26);
			}
			P2 = 0xF7;
			if(b==0)
			{
				num1[i] = '0';
				++i;
				cmd('0',1);
				delay(26);
			}
			if(c==0)
			{
				cmd('=',1);
				delay(26);
				answer(&num1, &num2, op);
			}
	}
	P2 = 0xFD;
	if(a==0)
	{
		num1[i] = '4';
		++i;
		cmd('4',1);
		delay(26);
	}
	if(b==0)
	{
		num1[i] = '5';
		++i;
		cmd('5',1);
		delay(26);
	}
	if(c==0)
	{
		num1[i] = '6';
		++i;
		cmd('6',1);
		delay(26);
	}
	if(d==0)
	{
		op = 'x';
		cmd('x',1);
		delay(26);
				i=0;
		P2 = 0xFE;
			if(a==0)
			{
				num2[i] = '7';
				++i;
				cmd('7',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '8';
				++i;
				cmd('8',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '9';
				++i;
				cmd('9',1);
				delay(26);
			}
			P2 = 0xFD;
			if(a==0)
			{
				num2[i] = '4';
				++i;
				cmd('4',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '5';
				++i;
				cmd('5',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '6';
				++i;
				cmd('6',1);
				delay(26);
			}
			P2 = 0xFB;
			if(a==0)
			{
				num2[i] = '1';
				++i;
				cmd('1',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '2';
				++i;
				cmd('2',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '3';
				++i;
				cmd('3',1);
				delay(26);
			}
			P2 = 0xF7;
			if(b==0)
			{
				num1[i] = '0';
				++i;
				cmd('0',1);
				delay(26);
			}
			if(c==0)
			{
				cmd('=',1);
				delay(26);
				answer(&num1, &num2, op);
			}
	}
	P2 = 0xFB;
	if(a==0)
	{
		num1[i] = '1';
		++i;
		cmd('1',1);
		delay(26);
	}
	if(b==0)
	{
		num1[i] = '2';
		++i;
		cmd('2',1);
		delay(26);
	}
	if(c==0)
	{
		num1[i] = '3';
		++i;
		cmd('3',1);
		delay(26);
	}
	if(d==0)
	{
		op = '-';
		cmd('-',1);
		delay(26);
				i=0;
		P2 = 0xFE;
			if(a==0)
			{
				num2[i] = '7';
				++i;
				cmd('7',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '8';
				++i;
				cmd('8',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '9';
				++i;
				cmd('9',1);
				delay(26);
			}
			P2 = 0xFD;
			if(a==0)
			{
				num2[i] = '4';
				++i;
				cmd('4',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '5';
				++i;
				cmd('5',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '6';
				++i;
				cmd('6',1);
				delay(26);
			}
			P2 = 0xFB;
			if(a==0)
			{
				num2[i] = '1';
				++i;
				cmd('1',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '2';
				++i;
				cmd('2',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '3';
				++i;
				cmd('3',1);
				delay(26);
			}
			P2 = 0xF7;
			if(b==0)
			{
				num1[i] = '0';
				++i;
				cmd('0',1);
				delay(26);
			}
			if(c==0)
			{
				cmd('=',1);
				delay(26);
				answer(&num1, &num2, op);
			}
	}
	P2 = 0xF7;
	if(a==0)
	{
		cmd(0x80,0);
		delay(26);
	}
	if(b==0)
	{
		num1[i] = '0';
		++i;
		cmd('0',1);
		delay(26);
	}
	if(c==0)
	{
		cmd('=',1);
		delay(26);
		answer(&num1, &num2, op);
	}
	if(d==0)
	{
		op = '+';
		cmd('+',1);
		delay(26);
				i=0;
		P2 = 0xFE;
			if(a==0)
			{
				num2[i] = '7';
				++i;
				cmd('7',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '8';
				++i;
				cmd('8',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '9';
				++i;
				cmd('9',1);
				delay(26);
			}
			P2 = 0xFD;
			if(a==0)
			{
				num2[i] = '4';
				++i;
				cmd('4',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '5';
				++i;
				cmd('5',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '6';
				++i;
				cmd('6',1);
				delay(26);
			}
			P2 = 0xFB;
			if(a==0)
			{
				num2[i] = '1';
				++i;
				cmd('1',1);
				delay(26);
			}
			if(b==0)
			{
				num2[i] = '2';
				++i;
				cmd('2',1);
				delay(26);
			}
			if(c==0)
			{
				num2[i] = '3';
				++i;
				cmd('3',1);
				delay(26);
			}
			P2 = 0xF7;
			if(b==0)
			{
				num1[i] = '0';
				++i;
				cmd('0',1);
				delay(26);
			}
			if(c==0)
			{
				cmd('=',1);
				delay(26);
				answer(&num1, &num2, op);
			}
	}
}
}